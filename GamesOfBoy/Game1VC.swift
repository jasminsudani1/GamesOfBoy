//
//  Game1VC.swift
//  GamesOfBoy
//
//  Created by Jasmin Sudani on 15/04/23.
//

import UIKit
import GoogleMobileAds

class Game1VC: UIViewController {
    
    private var interstitial: GADInterstitialAd?
    var bannerView: GADBannerView!
    @IBOutlet weak var lbl_Game_Name: UILabel!
    
    @IBOutlet weak var webView1: UIWebView!
    var game_Name : String!
    var url_game : String!
    var url_game_Name : String = "https://html5.gamedistribution.com/6d1c6b3a959b461587ebcac0b4d8fc56/?gdpr-targeting=1&gdpr-tracking=1&gdpr-third-party=1&euConsent=undefined&gd_sdk_referrer_url=https%3A%2F%2Fwww.bgames.com%2Fgame%2Fbridge-water-rush%2F"
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        lbl_Game_Name.text = game_Name
        
        
        webView1.loadRequest(URLRequest(url: URL(string: url_game_Name) ?? URL(string: "https://html5.gamedistribution.com/6d1c6b3a959b461587ebcac0b4d8fc56/?gdpr-targeting=1&gdpr-tracking=1&gdpr-third-party=1&euConsent=undefined&gd_sdk_referrer_url=https%3A%2F%2Fwww.bgames.com%2Fgame%2Fbridge-water-rush%2F")!))
        bannerView = GADBannerView(adSize: GADAdSizeBanner)
        
        addBannerViewToView(bannerView)
        bannerView.adUnitID = "ca-app-pub-1919696271600862/5995605042"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let request = GADRequest()
          GADInterstitialAd.load(withAdUnitID: "ca-app-pub-3940256099942544/4411468910",
                                      request: request,
                            completionHandler: { [self] ad, error in
//                              if let error = error {
//                                print("Failed to load interstitial ad with error: \(error.localizedDescription)")
//                                return
//                              }
                              interstitial = ad
                            }
          )
    }
    

    @IBAction func back_But(_ sender: UIButton) {
        
//        if interstitial != nil {
                    interstitial?.present(fromRootViewController: self)
        //          } else {
        //            print("Ad wasn't ready")
        //          }
        
        self.navigationController?.popViewController(animated: true)
    }
    

    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
          [NSLayoutConstraint(item: bannerView,
                              attribute: .bottom,
                              relatedBy: .equal,
                              toItem: view.safeAreaLayoutGuide,
                              attribute: .bottom,
                              multiplier: 1,
                              constant: 0),
           NSLayoutConstraint(item: bannerView,
                              attribute: .centerX,
                              relatedBy: .equal,
                              toItem: view,
                              attribute: .centerX,
                              multiplier: 1,
                              constant: 0)
          ])
       }
}
