//
//  GameListCollCell.swift
//  GamesOfBoy
//
//  Created by Jasmin Sudani on 01/05/23.
//

import UIKit

class GameListCollCell: UICollectionViewCell {
    
    @IBOutlet weak var image_View_List: UIImageView!
    
    @IBOutlet weak var lbl_Game_Name_list: UILabel!
    
    
}
