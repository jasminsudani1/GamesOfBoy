//
//  ViewController.swift
//  GamesOfBoy
//
//  Created by Jasmin Sudani on 14/04/23.
//
import GoogleMobileAds
import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var api = ApiFunc()
    
    @IBOutlet weak var game_Coll: UICollectionView!
    @IBOutlet weak var adebener: GADBannerView!
    @IBOutlet weak var bannerAde_View: GADBannerView!
    var bannerView: GADBannerView!
    private var interstitial: GADInterstitialAd?
    @IBOutlet weak var table_Game: UITableView!
    
//    var img_arr = [#imageLiteral(resourceName: "Bridge Water Rush"), #imageLiteral(resourceName: "Slap & Run.png"), #imageLiteral(resourceName: "Stack Colors.png"), #imageLiteral(resourceName: "Mob Control"), #imageLiteral(resourceName: "Tallman Run"), #imageLiteral(resourceName: "Dice Push.png")]
//    let game_Url_Arr = ["https://html5.gamedistribution.com/6d1c6b3a959b461587ebcac0b4d8fc56/?gdpr-targeting=1&gdpr-tracking=1&gdpr-third-party=1&euConsent=undefined&gd_sdk_referrer_url=https%3A%2F%2Fwww.bgames.com%2Fgame%2Fbridge-water-rush%2F","https://html5.gamedistribution.com/55bcc48daafe45bf9db2e85ecb64010b/?gdpr-targeting=1&gdpr-tracking=1&gdpr-third-party=1&euConsent=undefined&gd_sdk_referrer_url=https%3A%2F%2Fwww.bgames.com%2Fgame%2Fslap-run%2F","https://html5.gamedistribution.com/0810aaddf4ec4f1caf87023fab13a970/?gdpr-targeting=1&gdpr-tracking=1&gdpr-third-party=1&euConsent=undefined&gd_sdk_referrer_url=https%3A%2F%2Fwww.bgames.com%2Fgame%2Fstack-colors%2F","https://html5.gamedistribution.com/1d9e7e2883e144d293c0894e230b5441/?gdpr-targeting=1&gdpr-tracking=1&gdpr-third-party=1&euConsent=undefined&gd_sdk_referrer_url=https%3A%2F%2Fwww.bgames.com%2Fgame%2Fmob-control%2F","https://html5.gamedistribution.com/7980c23fbbae4af6851e01052fce3cce/?gdpr-targeting=1&gdpr-tracking=1&gdpr-third-party=1&euConsent=undefined&gd_sdk_referrer_url=https%3A%2F%2Fwww.bgames.com%2Fgame%2Ftallman-run%2F","https://html5.gamedistribution.com/a89d3fe9148344c9b4d2fb966eb41319/?gdpr-targeting=1&gdpr-tracking=1&gdpr-third-party=1&euConsent=undefined&gd_sdk_referrer_url=https%3A%2F%2Fwww.bgames.com%2Fgame%2Fdice-push%2F"]
//    var text_Arr = ["Bridge Water Rush","Slap & Run","Stack Colors","Mob Control","Tallman Run","Dice Push"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        table_Game.dataSource = self
//        table_Game.delegate = self
        
        game_Coll.dataSource = self
        game_Coll.delegate = self
        
        bannerView = GADBannerView(adSize: GADAdSizeBanner)
        
        addBannerViewToView(bannerView)
        bannerView.adUnitID = "ca-app-pub-1919696271600862/5995605042"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
        let request = GADRequest()
          GADInterstitialAd.load(withAdUnitID: "ca-app-pub-3940256099942544/4411468910",
                                      request: request,
                            completionHandler: { [self] ad, error in
//                              if let error = error {
//                                print("Failed to load interstitial ad with error: \(error.localizedDescription)")
//                                return
//                              }
                              interstitial = ad
                            }
          )

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let request = GADRequest()
          GADInterstitialAd.load(withAdUnitID: "ca-app-pub-3940256099942544/4411468910",
                                      request: request,
                            completionHandler: { [self] ad, error in
//                              if let error = error {
//                                print("Failed to load interstitial ad with error: \(error.localizedDescription)")
//                                return
//                              }
                              interstitial = ad
                            }
          )
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return api.text_Arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let x = collectionView.dequeueReusableCell(withReuseIdentifier: "GameListCollCell", for: indexPath) as! GameListCollCell
        
        x.image_View_List.image = api.img_arr[indexPath.row]
        x.lbl_Game_Name_list.text = api.text_Arr[indexPath.row]
        
        return x
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var a1 : Game1VC  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "Game1VC") as! Game1VC
//        a1.game_Name = text_Arr[indexPath.row]
        a1.url_game_Name = api.game_Url_Arr[indexPath.row]
        
//        if interstitial != nil {
            interstitial?.present(fromRootViewController: self)
//          } else {
//            print("Ad wasn't ready")
//          }
        
        self.navigationController?.pushViewController(a1, animated: true)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return text_Arr.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let x = tableView.dequeueReusableCell(withIdentifier: "GameTableViewCell") as! GameTableViewCell
//
//        x.img_Game.image = img_arr[indexPath.row]
//        x.lbl_Game_Name.text = text_Arr[indexPath.row]
//
//        return x
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        var a1 : Game1VC  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "Game1VC") as! Game1VC
////        a1.game_Name = text_Arr[indexPath.row]
//        a1.url_game_Name = game_Url_Arr[indexPath.row]
//
////        if interstitial != nil {
//            interstitial?.present(fromRootViewController: self)
////          } else {
////            print("Ad wasn't ready")
////          }
//
//        self.navigationController?.pushViewController(a1, animated: true)
//    }
//
//
//
//    @IBAction func But_1(_ sender: UIButton) {
//
//       let x = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "Game1VC")
//
//        self.navigationController?.pushViewController(x, animated: true)
//
//    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
          [NSLayoutConstraint(item: bannerView,
                              attribute: .bottom,
                              relatedBy: .equal,
                              toItem: view.safeAreaLayoutGuide,
                              attribute: .bottom,
                              multiplier: 1,
                              constant: 0),
           NSLayoutConstraint(item: bannerView,
                              attribute: .centerX,
                              relatedBy: .equal,
                              toItem: view,
                              attribute: .centerX,
                              multiplier: 1,
                              constant: 0)
          ])
       }
   
}

